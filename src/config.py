batch_size = 8
loss = fn
metrics = fn

regularization_weight= 1e-5,
learning_rate_reduce_rate= 0.5,
learning_rate_reduce_patience= 5,
early_stopping_patience= 5,
optimizer= OptimizerType.Adam,

data_path = ''


learning_rate = 1e-3
epochs = 100
callbacks = []